import { AdComponent } from './../ad.interface';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-viny',
  templateUrl: './viny.component.html',
  styleUrls: ['./viny.component.scss']
})
export class VinyComponent implements OnInit, AdComponent {
  @Input() data: any;
  constructor() { }

  ngOnInit(): void {
  }

}
