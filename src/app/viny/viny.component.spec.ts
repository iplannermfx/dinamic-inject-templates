import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VinyComponent } from './viny.component';

describe('VinyComponent', () => {
  let component: VinyComponent;
  let fixture: ComponentFixture<VinyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VinyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VinyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
