import { VinyComponent } from './viny/viny.component';
import { FormControl, Validators } from '@angular/forms';
import { AdItem } from './ad-item';
import { TextComponent } from './text/text.component';
import { Component, ViewChild, AfterContentInit, ComponentFactoryResolver, ViewContainerRef, ReflectiveInjector, Injector } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'dinamic-template';
  list: AdItem[];

  constructor() {
    this.list = [
      new AdItem(TextComponent, {formControl: new FormControl('xpto')}),
      new AdItem(TextComponent, {formControl: new FormControl('', [Validators.required, Validators.email])}),
      new AdItem(VinyComponent, {formControl: new FormControl('')})
    ];
  }
}
