import { AdDirective } from './directives/ad.directive';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TextComponent } from './text/text.component';
import { DropdownComponent } from './dropdown/dropdown.component';
import { ContainerBuildComponent } from './container-build/container-build.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { VinyComponent } from './viny/viny.component';
@NgModule({
  declarations: [
    AppComponent,
    TextComponent,
    DropdownComponent,
    ContainerBuildComponent,
    AdDirective,
    VinyComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
