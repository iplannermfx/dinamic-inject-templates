import { AdComponent } from './../ad.interface';
import { Component, OnInit, Input } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-text',
  templateUrl: './text.component.html',
  styleUrls: ['./text.component.scss']
})
export class TextComponent implements OnInit, AdComponent {
  @Input() set data(d) {
    const { formControl } = d;
    if (!formControl) {
      return;
    }
    this.formControl = formControl;
  }
  formControl: FormControl;
  constructor() { }

  ngOnInit(): void {
  }

}
