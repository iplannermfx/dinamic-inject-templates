import { AdItem } from './../ad-item';
import { AdComponent } from './../ad.interface';
import { AdDirective } from './../directives/ad.directive';
import { Component, OnInit, ViewChild, Input, ComponentFactoryResolver } from '@angular/core';
import { FormControl, Validators, FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-container-build',
  templateUrl: './container-build.component.html',
  styleUrls: ['./container-build.component.scss']
})
export class ContainerBuildComponent {
  @Input() set item(i) {
    if (!i) {
      return;
    }
    this.loadMyComponent(i)
  };
  @ViewChild(AdDirective, {static: true}) adHost: AdDirective;

  constructor(private componentFactoryResolver: ComponentFactoryResolver) {}

  loadMyComponent (item) {
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(item.component);

    const viewContainerRef = this.adHost.viewContainerRef;
    viewContainerRef.clear();

    const componentRef = viewContainerRef.createComponent<AdComponent>(componentFactory as any);
    componentRef.instance.data = item.data;
  }
}
